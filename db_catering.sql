-- phpMyAdmin SQL Dump
-- version 4.4.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 03, 2019 at 04:23 PM
-- Server version: 5.6.26
-- PHP Version: 5.6.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_catering`
--

-- --------------------------------------------------------

--
-- Table structure for table `invoices`
--

CREATE TABLE IF NOT EXISTS `invoices` (
  `id` int(10) NOT NULL,
  `date` datetime NOT NULL,
  `due_date` datetime NOT NULL,
  `status` varchar(100) NOT NULL,
  `bukti` varchar(100) NOT NULL,
  `nm_pelanggan` varchar(50) NOT NULL,
  `alamat` varchar(100) NOT NULL,
  `nohp` varchar(13) NOT NULL,
  `email` varchar(50) NOT NULL,
  `tgl_kirim` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=310 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `invoices`
--

INSERT INTO `invoices` (`id`, `date`, `due_date`, `status`, `bukti`, `nm_pelanggan`, `alamat`, `nohp`, `email`, `tgl_kirim`) VALUES
(240, '2019-07-20 11:16:56', '2019-07-21 11:16:56', 'Paid', 'buktitf2.jpg', 'Fitrindha nurwulan', 'Jalan garuda no.19 Bandung', '0877889999976', 'Fitrindhawulan@gmail.com', '2019-07-20'),
(241, '2019-07-20 11:45:08', '2019-07-21 11:45:08', 'Paid', 'IMG_5413.JPG', 'Sherlin', 'Jalan garuda no.19 Bandung', '081235556222', 'fitrinda12@gmail.com', '2019-07-20'),
(242, '2019-07-20 11:59:08', '2019-07-21 11:59:08', 'Paid', 'IMG_5412.JPG', 'Alya dianda', 'Jalan Rajawali no.2 Bandung', '081255572228', 'Fitrindhawulan@gmail.com', '2019-07-20'),
(243, '2019-07-21 05:42:56', '2019-07-22 05:42:56', 'Paid', 'Belum ada', 'Viana Risanti', 'Jalan garuda no.19 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-07-21'),
(244, '2019-07-22 10:20:53', '2019-07-23 10:20:53', 'Paid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan garuda no.19 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-07-22'),
(245, '2019-07-24 12:05:44', '2019-07-25 12:05:44', 'Paid', 'Belum ada', 'Nyobainn', 'Jalan Rajawali no.2 Bandung', '081255572228', 'Fitrindhawulan@gmail.com', '2019-07-24'),
(246, '2019-07-24 12:24:26', '2019-07-25 12:24:26', 'Paid', 'bukti.JPG', 'Sherlin', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-07-31'),
(247, '2019-07-25 05:30:22', '2019-07-26 05:30:22', 'Paid', 'bukti1.JPG', 'Fitrindha nurwulan', 'Jalan garuda no.19 Bandung', '081255572228', 'fitrinda12@gmail.com', '2019-07-31'),
(248, '2019-07-26 06:25:26', '2019-07-27 06:25:26', 'Unpaid', 'Belum ada', 'Nyobainn', 'Jalan Halteu Utara no.191 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-07-31'),
(249, '2019-07-26 06:30:55', '2019-07-27 06:30:55', 'Unpaid', 'Belum ada', 'Nyobainn', 'Jalan Halteu Utara no.191 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-07-31'),
(250, '2019-07-26 06:49:54', '2019-07-27 06:49:54', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-07-31'),
(251, '2019-07-26 06:50:47', '2019-07-27 06:50:47', 'Unpaid', 'Belum ada', 'Nyobainn', 'Bandung', '99999', 'Fitrindhawulan@gmail.com', '2019-07-31'),
(252, '2019-07-26 06:52:56', '2019-07-27 06:52:56', 'Unpaid', 'Belum ada', 'Nyobainn', 'Bandung', '99999', 'Fitrindhawulan@gmail.com', '2019-07-31'),
(253, '2019-07-26 06:53:28', '2019-07-27 06:53:28', 'Unpaid', 'Belum ada', 'Nyobainn', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-07-31'),
(254, '2019-07-26 07:05:46', '2019-07-27 07:05:46', 'Unpaid', 'Belum ada', 'Nyobainn', 'Jalan Halteu Utara no.191 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-07-31'),
(255, '2019-07-27 06:21:06', '2019-07-28 06:21:06', 'Unpaid', 'Belum ada', 'Nyobainn', 'Jalan garuda no.19 Bandung', '081255572228', 'Fitrindhawulan@gmail.com', '2019-07-31'),
(256, '2019-07-27 06:24:17', '2019-07-28 06:24:17', 'Paid', 'Belum ada', 'Nyobainn', 'Jalan garuda no.19 Bandung', '081255572228', 'Fitrindhawulan@gmail.co', '2019-07-31'),
(257, '2019-07-27 06:24:35', '2019-07-28 06:24:35', 'Unpaid', 'Belum ada', 'Nyobainn', 'Jalan garuda no.19 Bandung', '081255572228', 'Fitrindhawulannnnnn@gmail.com', '2019-07-31'),
(258, '2019-07-27 06:25:58', '2019-07-28 06:25:58', 'Unpaid', 'Belum ada', 'Nyobainn', 'Jalan garuda no.19 Bandung', '081255572228', 'nyobbnj@gmail.com', '2019-07-30'),
(259, '2019-07-27 06:31:25', '2019-07-28 06:31:25', 'Unpaid', 'Belum ada', 'Nyobainn', 'Jalan Halteu Utara no.191 Bandung', '081255572228', 'nyobbnj@gmail.com', '2019-07-31'),
(260, '2019-07-28 15:51:42', '2019-07-29 15:51:42', 'Paid', 'bukti2.JPG', 'Muh Dimmas', 'Jalan Rajawali no.2 Bandung', '081234670087', 'Fitrindhawulan@gmail.com', '2019-07-31'),
(261, '2019-07-29 05:05:16', '2019-07-30 05:05:16', 'Paid', 'bukti3.JPG', 'Anne nursyifa', 'jalan sariasih no.54', '08778899999', 'Fitrindhawulan@gmail.com', '2019-07-31'),
(262, '2019-07-29 06:17:44', '2019-07-30 06:17:44', 'Paid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Halteu Utara no.191 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-07-31'),
(263, '2019-07-30 15:29:30', '2019-07-31 15:29:30', 'Unpaid', 'bukti4.JPG', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-01'),
(264, '2019-07-31 06:47:29', '2019-08-01 06:47:29', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-16'),
(265, '2019-07-31 06:48:31', '2019-08-01 06:48:31', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-16'),
(266, '2019-07-31 06:49:23', '2019-08-01 06:49:23', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-02'),
(267, '2019-07-31 06:59:41', '2019-08-01 06:59:41', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-02'),
(268, '2019-07-31 07:00:17', '2019-08-01 07:00:17', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '081255572228', 'Fitrindhawulan@gmail.com', '2019-08-01'),
(269, '2019-07-31 07:17:37', '2019-08-01 07:17:37', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-02'),
(270, '2019-07-31 08:24:01', '2019-08-01 08:24:01', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(271, '2019-07-31 08:25:46', '2019-08-01 08:25:46', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(272, '2019-07-31 08:26:25', '2019-08-01 08:26:25', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(273, '2019-07-31 08:47:30', '2019-08-01 08:47:30', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(274, '2019-07-31 12:03:59', '2019-08-01 12:03:59', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(275, '2019-07-31 12:11:24', '2019-08-01 12:11:24', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(276, '2019-07-31 12:19:04', '2019-08-01 12:19:04', 'Unpaid', 'Belum ada', 'Sherlin', 'Jalan garuda no.19 Bandung', '081234670087', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(277, '2019-07-31 13:41:22', '2019-08-01 13:41:22', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Halteu Utara no.191 Bandung', '081234670087', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(278, '2019-07-31 13:42:38', '2019-08-01 13:42:38', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan garuda no.19 Bandung', '081255572228', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(279, '2019-07-31 13:47:42', '2019-08-01 13:47:42', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '081234670087', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(280, '2019-07-31 13:49:50', '2019-08-01 13:49:50', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '081234670087', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(281, '2019-07-31 13:50:15', '2019-08-01 13:50:15', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '081255572228', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(282, '2019-07-31 13:56:11', '2019-08-01 13:56:11', 'Unpaid', 'Belum ada', 'Alya dianda', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(283, '2019-07-31 13:59:51', '2019-08-01 13:59:51', 'Unpaid', 'Belum ada', 'Alya dianda', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(284, '2019-07-31 14:00:21', '2019-08-01 14:00:21', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '081234670087', 'Fitrindhawulan@gmail.com', '2019-08-02'),
(285, '2019-07-31 14:12:04', '2019-08-01 14:12:04', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Halteu Utara no.191 Bandung', '081255572228', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(286, '2019-07-31 16:23:50', '2019-08-01 16:23:50', 'Unpaid', 'Belum ada', 'Sherlin', 'Jalan Rajawali no.2 Bandung', '081255572228', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(287, '2019-07-31 16:42:35', '2019-08-01 16:42:35', 'Unpaid', 'Belum ada', 'Alya dianda', 'Jalan Halteu Utara no.191 Bandung', '081234670087', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(288, '2019-07-31 16:44:02', '2019-08-01 16:44:02', 'Unpaid', 'Belum ada', 'Sherlin', 'Jalan Rajawali no.2 Bandung', '081234670087', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(289, '2019-07-31 16:46:33', '2019-08-01 16:46:33', 'Unpaid', 'Belum ada', 'Sherlin', 'Jalan Rajawali no.2 Bandung', '081234670087', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(290, '2019-07-31 16:47:05', '2019-08-01 16:47:05', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '081255572228', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(291, '2019-07-31 17:03:26', '2019-08-01 17:03:26', 'Unpaid', 'Belum ada', 'Alya dianda', 'Jalan garuda no.19 Bandung', '08778899999', 'fitrinda12@gmail.com', '2019-08-03'),
(292, '2019-07-31 17:07:00', '2019-08-01 17:07:00', 'Unpaid', 'Belum ada', 'Alya dianda', 'Jalan Rajawali no.2 Bandung', '081255572228', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(293, '2019-07-31 17:17:07', '2019-08-01 17:17:07', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(294, '2019-07-31 17:28:50', '2019-08-01 17:28:50', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(295, '2019-07-31 17:29:34', '2019-08-01 17:29:34', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan garuda no.19 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-07-31'),
(296, '2019-08-01 05:57:52', '2019-08-02 05:57:52', 'Unpaid', 'Belum ada', 'Alya dianda', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-10'),
(297, '2019-08-01 06:01:30', '2019-08-02 06:01:30', 'Unpaid', 'Belum ada', 'Alya dianda', 'Jalan garuda no.19 Bandung', '081255572228', 'Fitrindhawulan@gmail.com', '2019-08-10'),
(298, '2019-08-01 06:12:32', '2019-08-02 06:12:32', 'Unpaid', 'Belum ada', 'Nurwulan', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(299, '2019-08-01 06:23:30', '2019-08-02 06:23:30', 'Unpaid', 'Belum ada', 'Viana Risanti', 'Jalan garuda no.19 Bandung', '081234670087', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(300, '2019-08-01 06:30:44', '2019-08-02 06:30:44', 'Unpaid', 'Belum ada', 'Sherlin', 'Jalan Halteu Utara no.191 Bandung', '081255572228', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(301, '2019-08-01 06:33:44', '2019-08-02 06:33:44', 'Unpaid', 'Belum ada', 'Sherlin', 'Jalan Halteu Utara no.191 Bandung', '081255572228', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(302, '2019-08-01 06:34:10', '2019-08-02 06:34:10', 'Unpaid', 'Belum ada', 'Alya dianda', 'Jalan garuda no.19 Bandung', '081255572228', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(303, '2019-08-01 06:38:42', '2019-08-02 06:38:42', 'Unpaid', 'Belum ada', 'Sherlin', 'Jalan garuda no.19 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-03'),
(304, '2019-08-02 06:34:18', '2019-08-03 06:34:18', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-10'),
(305, '2019-08-02 06:39:02', '2019-08-03 06:39:02', 'Unpaid', 'Belum ada', 'Nyobainn', 'Jalan Halteu Utara no.191 Bandung', '0811111111', 'Fitrindhawulan@gmail.com', '2019-08-10'),
(306, '2019-08-02 06:41:21', '2019-08-03 06:41:21', 'Unpaid', 'Belum ada', 'Nyobainn', 'Jalan Rajawali no.2 Bandung', '081234670087', 'Fitrindhawulan@gmail.com', '0000-00-00'),
(307, '2019-08-02 06:44:17', '2019-08-03 06:44:17', 'Unpaid', 'Belum ada', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '0000-00-00'),
(308, '2019-08-02 06:46:33', '2019-08-03 06:46:33', 'Unpaid', 'Belum ada', 'Nyobainn', 'Jalan Rajawali no.2 Bandung', '08778899999', 'Fitrindhawulan@gmail.com', '2019-08-10'),
(309, '2019-08-02 06:50:37', '2019-08-03 06:50:37', 'Paid', 'bukti5.JPG', 'Fitrindha nurwulan', 'Jalan Rajawali no.2 Bandung', '081255572228', 'Fitrindhawulan@gmail.com', '2019-08-10');

-- --------------------------------------------------------

--
-- Table structure for table `kategori`
--

CREATE TABLE IF NOT EXISTS `kategori` (
  `id` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `gambar` varchar(100) NOT NULL,
  `nm_makanan` varchar(50) NOT NULL,
  `deskripsi` varchar(250) NOT NULL,
  `harga` int(30) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`gambar`, `nm_makanan`, `deskripsi`, `harga`, `kode`, `id`) VALUES
('ikanbakar16.jpg', 'Ikan Bakar', 'ikan yang dibakar dengan bumbu dan rempah rempah yang berkualitas', 20000, 'IK01', 1),
('rendang4.jpg', 'Rendang Padang', 'Daging pilihan dengan bumbu khas masakan padang', 25000, 'RN01', 4);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) NOT NULL,
  `invoice_id` int(10) NOT NULL,
  `product_id` int(10) NOT NULL,
  `product_name` varchar(50) NOT NULL,
  `qty` int(3) NOT NULL,
  `price` int(9) NOT NULL,
  `total` int(12) NOT NULL,
  `date` datetime NOT NULL,
  `nm_pelanggan` varchar(100) NOT NULL,
  `sub_total` int(20) NOT NULL,
  `tgl_kirim` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=318 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `invoice_id`, `product_id`, `product_name`, `qty`, `price`, `total`, `date`, `nm_pelanggan`, `sub_total`, `tgl_kirim`) VALUES
(256, 240, 21, 'Ayam', 1, 20000, 20000, '2019-06-01 11:16:56', 'Fitrindha nurwulan', 20000, '2019-06-05'),
(257, 241, 30, 'Nasi Goreng spesial', 20, 15000, 420000, '2019-05-08 11:45:08', 'Sherlin', 300000, '2019-05-17'),
(258, 241, 18, 'Nasi Goreng Rempah-Rempah', 10, 12000, 420000, '2019-04-24 11:45:08', 'Sherlin', 120000, '2019-04-26'),
(259, 242, 27, 'Ikan Bakar', 150, 25000, 4875000, '2019-07-20 11:59:09', 'Alya dianda', 3750000, '2019-07-31'),
(260, 242, 29, 'Soto Bandung', 75, 15000, 4875000, '2019-07-20 11:59:09', 'Alya dianda', 1125000, '2019-07-31'),
(261, 243, 18, 'Nasi Goreng Rempah-Rempah', 1, 12000, 12000, '2019-07-21 05:42:56', 'Viana Risanti', 12000, '2019-07-31'),
(262, 244, 32, 'jengkol', 100, 10000, 2200000, '2019-03-07 10:20:53', 'Fitrindha nurwulan', 1000000, '2019-03-22'),
(263, 244, 18, 'Nasi Goreng Rempah-Rempah', 100, 12000, 2200000, '2019-07-22 10:20:53', 'Fitrindha nurwulan', 1200000, '2019-07-31'),
(264, 0, 28, 'Ayam Bakar', 1000, 20000, 20000000, '2019-07-22 16:11:43', 'Fitrindha nurwulan', 20000000, '2019-07-31'),
(265, 245, 30, 'Nasi Goreng spesial', 1000, 15000, 15000000, '2019-02-21 12:05:44', 'Nyobainn', 15000000, '2019-01-30'),
(266, 246, 30, 'Nasi Goreng spesial', 1, 15000, 15000, '2019-07-24 12:24:27', 'Sherlin', 15000, '2019-07-31'),
(267, 247, 32, 'jengkol', 1000, 10000, 85000000, '2019-07-25 05:30:22', 'Fitrindha nurwulan', 10000000, '2019-07-31'),
(268, 247, 29, 'Soto Bandung', 5000, 15000, 85000000, '2019-07-25 05:30:23', 'Fitrindha nurwulan', 75000000, '2019-07-31'),
(269, 249, 26, 'Rendang Padang', 1000, 25000, 25000000, '2019-01-08 06:30:55', 'Nyobainn', 25000000, '2019-01-16'),
(270, 251, 26, 'Rendang Padang', 1, 25000, 25000, '2019-07-26 06:50:47', 'Nyobainn', 25000, '2019-07-31'),
(271, 253, 27, 'Ikan Bakar', 1, 25000, 25000, '2019-07-26 06:53:28', 'Nyobainn', 25000, '2019-07-31'),
(272, 254, 27, 'Ikan Bakar', 1, 25000, 25000, '2019-07-26 07:05:46', 'Nyobainn', 25000, '2019-07-31'),
(273, 255, 27, 'Ikan Bakar', 1, 25000, 25000, '2019-07-27 06:21:06', 'Nyobainn', 25000, '2019-07-31'),
(274, 259, 28, 'Ayam Bakar', 100000, 20000, 2000000000, '2019-07-27 06:31:25', 'Nyobainn', 2000000000, '2019-07-31'),
(275, 260, 34, 'ayam bakar spesial', 10000, 25000, 250000000, '2019-07-28 15:51:42', 'Muh Dimmas', 250000000, '2019-07-31'),
(276, 261, 35, 'Ikan Bakar', 1000, 15000, 264975000, '2019-07-29 05:05:17', 'Anne nursyifa', 15000000, '2019-07-31'),
(277, 261, 34, 'ayam bakar spesial', 9999, 25000, 264975000, '2019-07-29 05:05:17', 'Anne nursyifa', 249975000, '2019-07-31'),
(278, 262, 34, 'ayam bakar spesial', 2, 25000, 50000, '2019-07-29 06:17:44', 'Fitrindha nurwulan', 50000, '2019-07-31'),
(279, 263, 1, 'Ikan Bakar', 1, 15000, 15000, '2019-07-30 15:29:30', 'Fitrindha nurwulan', 15000, '2019-08-01'),
(280, 264, 34, 'ayam bakar spesial', 1, 25000, 25000, '2019-07-31 06:47:29', 'Fitrindha nurwulan', 25000, '2019-08-16'),
(281, 266, 34, 'ayam bakar spesial', 1, 25000, 25000, '2019-07-31 06:49:23', 'Fitrindha nurwulan', 25000, '2019-08-02'),
(282, 268, 38, 'Ikan Bakar', 1, 20000, 20000, '2019-07-31 07:00:17', 'Fitrindha nurwulan', 20000, '2019-08-01'),
(283, 269, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-07-31 07:17:37', 'Fitrindha nurwulan', 20000, '2019-08-02'),
(284, 270, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-07-31 08:24:02', 'Fitrindha nurwulan', 20000, '2019-08-03'),
(285, 272, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-07-31 08:26:25', 'Fitrindha nurwulan', 20000, '2019-08-03'),
(286, 273, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-07-31 08:47:30', 'Fitrindha nurwulan', 20000, '2019-08-03'),
(287, 274, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-07-31 12:04:00', 'Fitrindha nurwulan', 20000, '2019-08-03'),
(288, 275, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-07-31 12:11:24', 'Fitrindha nurwulan', 20000, '2019-08-03'),
(289, 276, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-07-31 12:19:04', 'Sherlin', 20000, '2019-08-03'),
(290, 277, 1, 'Ikan Bakar', 1, 15000, 15000, '2019-07-31 13:41:22', 'Fitrindha nurwulan', 15000, '2019-08-03'),
(291, 278, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-07-31 13:42:38', 'Fitrindha nurwulan', 20000, '2019-08-03'),
(292, 279, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-07-31 13:47:42', 'Fitrindha nurwulan', 20000, '2019-08-03'),
(293, 281, 1, 'Ikan Bakar', 1, 15000, 15000, '2019-07-31 13:50:15', 'Fitrindha nurwulan', 15000, '2019-08-03'),
(294, 282, 1, 'Ikan Bakar', 1, 15000, 15000, '2019-07-31 13:56:11', 'Alya dianda', 15000, '2019-08-03'),
(295, 284, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-07-31 14:00:21', 'Fitrindha nurwulan', 20000, '2019-08-02'),
(296, 285, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-07-31 14:12:04', 'Fitrindha nurwulan', 20000, '2019-08-03'),
(297, 286, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-07-31 16:23:50', 'Sherlin', 20000, '2019-08-03'),
(298, 287, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-07-31 16:42:35', 'Alya dianda', 20000, '2019-08-03'),
(299, 288, 1, 'Ikan Bakar', 1, 15000, 15000, '2019-07-31 16:44:02', 'Sherlin', 15000, '2019-08-03'),
(300, 290, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-07-31 16:47:05', 'Fitrindha nurwulan', 20000, '2019-08-03'),
(301, 291, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-07-31 17:03:26', 'Alya dianda', 20000, '2019-08-03'),
(302, 292, 1, 'Ikan Bakar', 1, 15000, 15000, '2019-07-31 17:07:01', 'Alya dianda', 15000, '2019-08-03'),
(303, 293, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-07-31 17:17:07', 'Fitrindha nurwulan', 20000, '2019-08-03'),
(304, 295, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-07-31 17:29:34', 'Fitrindha nurwulan', 20000, '2019-07-31'),
(305, 296, 1, 'Ikan Bakar', 1, 15000, 15000, '2019-08-01 05:57:52', 'Alya dianda', 15000, '2019-08-10'),
(306, 297, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-08-01 06:01:30', 'Alya dianda', 20000, '2019-08-10'),
(307, 298, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-08-01 06:12:32', 'Nurwulan', 20000, '2019-08-03'),
(308, 299, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-08-01 06:23:30', 'Viana Risanti', 20000, '2019-08-03'),
(309, 300, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-08-01 06:30:44', 'Sherlin', 20000, '2019-08-03'),
(310, 302, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-08-01 06:34:10', 'Alya dianda', 20000, '2019-08-03'),
(311, 303, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-08-01 06:38:42', 'Sherlin', 20000, '2019-08-03'),
(312, 304, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-08-02 06:34:18', 'Fitrindha nurwulan', 20000, '2019-08-10'),
(313, 305, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-08-02 06:39:02', 'Nyobainn', 20000, '2019-08-10'),
(314, 306, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-08-02 06:41:21', 'Nyobainn', 20000, '0000-00-00'),
(315, 307, 38, 'Ikan Bakar', 1, 20000, 20000, '2019-08-02 06:44:17', 'Fitrindha nurwulan', 20000, '0000-00-00'),
(316, 308, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-08-02 06:46:33', 'Nyobainn', 20000, '2019-08-10'),
(317, 309, 36, 'ayam bakar spesial', 1, 20000, 20000, '2019-08-02 06:50:38', 'Fitrindha nurwulan', 20000, '2019-08-10');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `level` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `nama`, `username`, `password`, `level`) VALUES
(1, 'admin', 'admin', 'admin123', 'Admin'),
(2, 'Manager', 'manager', 'manager123', 'Pemilik'),
(3, 'keuangan', 'keuangan', 'keuangan123', 'Keuangan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `invoices`
--
ALTER TABLE `invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`kode`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `invoices`
--
ALTER TABLE `invoices`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=310;
--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=318;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
